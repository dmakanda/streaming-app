import os
import click
COV = None
if os.environ.get('COVERAGE'):
    import coverage
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()
from app import create_app, db
from app.models import User, Follow, Video, Comment, Playlist, Note, Transaction, Payment, Category

from flask_migrate import Migrate

app = create_app(os.getenv('CONFIG') or 'default')
migrate = Migrate(app, db)

@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db, User=User, Follow=Follow, Video=Video, Comment=Comment, Playlist=Playlist, Note=Note,Category=Category, Transaction=Transaction, Payment=Payment)

@app.cli.command()
def deploy():
    from flask_migrate import upgrade
    from app.models import User
    upgrade()
    User.add_self_follows()


@app.cli.command()
@click.option('--length', default=25, help='Length of the profile')
@click.option('--profile-dir', default=None, help='Directory to save the profile')
def profile(length=25, profile_dir=None):
    from werkzeug.contrib.profiler import ProfilerMiddleware
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[length], profile_dir=profile_dir)
    app.run()


@app.cli.command()
@click.option('--coverage/--no-coverage', default=False, help='Run tests under code coverage.')
def test(coverage=False):
    if coverage and not os.environ.get('COVERAGE'):
        import sys
        os.environ['COVERAGE'] = '1'
        os.execvp(sys.executable, [sys.executable] + sys.argv)
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

    if COV:
        COV.stop()
        COV.save()
        print('Coverage summary.')
        COV.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tmp/coverage')
        COV.html_report(directory=covdir)
        print('HTML version: file://%s/index.html' % covdir)
        COV.erase()


if os.path.exists('.env'):
    print('import env variables')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]
            

if __name__ == "__main__":
    app = create_app(os.getenv('CONFIG') or 'default')
    app.run(host='0.0.0.0', port=8000, threaded=True)