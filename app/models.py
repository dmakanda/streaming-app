import hashlib
from markdown import markdown
import bleach
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, AnonymousUserMixin
from . import login_manager, db
from itsdangerous import URLSafeTimedSerializer as Serializer
from flask import current_app, request, url_for
from .exceptions import ValidationError

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))



class Follow(db.Model):
    __tablename__ = 'follows'
    follower_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    followed_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)


class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    videos = db.relationship('Video', backref='category', lazy='dynamic')
    
    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'videos': [video.to_json() for video in self.videos]
        }


class Note(db.Model):
    __tablename__ = 'notes'
    id = db.Column(db.Integer, primary_key=True)
    rating = db.Column(db.Integer)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    video_id = db.Column(db.Integer, db.ForeignKey('videos.id'))

    def to_json(self):
        json_note = {
            # 'url': url_for('api.get_note', id=self.id, _external=True),
            'id': self.id,
            'rating': self.rating,
            'author_id': self.author_id,
            'video_id': self.video_id,
            # 'author': url_for('api.get_user', id=self.author_id, _external=True),
            # 'video': url_for('api.get_video', id=self.video_id, _external=True)
        }
        return json_note


class Comment(db.Model):
    __tablename__ = 'comments'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    body_html = db.Column(db.Text)
    disabled = db.Column(db.Boolean)
    video_id = db.Column(db.Integer, db.ForeignKey('videos.id'))

    def to_json(self):
        json_comment = {
            'url': url_for('api.get_comments', id=self.id, _external=True),
            'body': self.body,
            'body_html': self.body_html,
            'timestamp': self.timestamp,
            'disabled': self.disabled,
            'author': url_for('api.get_user', id=self.author_id, _external=True),
            'video': url_for('api.get_video', id=self.video_id, _external=True)
        }
        return json_comment

    @staticmethod
    def on_changed_body(target, value, oldvalue, initiator):
        allowed_tags = ['a', 'abbr', 'acronym', 'b', 'code', 'em', 'i', 'strong']
        target.body_html = bleach.linkify(bleach.clean(markdown(value, output_format='html'), tags=allowed_tags, strip=True))


class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    video_id = db.Column(db.Integer, db.ForeignKey('videos.id'))

    def to_json(self):
        json_transaction = {
            # 'url': url_for('api.get_transaction', id=self.id, _external=True),
            'id': self.id,
            'amount': self.amount,
            'timestamp': self.timestamp,
            'author_id': self.author_id,
            'video_id': self.video_id,
            # 'author': url_for('api.get_user', id=self.author_id, _external=True),
            # 'video': url_for('api.get_video', id=self.video_id, _external=True)
        }
        return json_transaction


class Payment(db.Model):
    __tablename__ = 'payments'
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    session_id = db.Column(db.Integer)
    mode = db.Column(db.String(64), default="payment")
    payment_method_types = db.Column(db.String(64))

    def to_json(self):
        json_payment = {
            # 'url': url_for('api.get_payment', id=self.id, _external=True),
            'id': self.id,
            'session_id': self.session_id,
            'timestamp': self.timestamp,
            'mode': self.mode,
            'payment_method_types': self.payment_method_types,
        }
        return json_payment


class Playlist(db.Model):
    __tablename__ = 'playlists'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    videos = db.relationship('Video', backref='playlist', lazy='dynamic')
    is_private = db.Column(db.Boolean, default=False)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def to_json(self):
        json_playlist = {
            # 'url': url_for('api.get_playlist', id=self.id, _external=True),
            'id': self.id,
            'name': self.name,
            'user_id': self.user_id,
            #'user': url_for('api.get_user', id=self.user_id, _external=True),
            'videos': [video for video in self.videos],
            # 'videos': [url_for('api.get_video', id=video.id, _external=True) for video in self.videos],
            'is_private': self.is_private,
            'timestamp': self.timestamp
        }
        return json_playlist


class User(UserMixin, db.Model):
    __tablename__  = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=False)
    birthdate = db.Column(db.DateTime())
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    location = db.Column(db.String(64))
    about_me = db.Column(db.Text())
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)
    last_seen = db.Column(db.DateTime(), default=datetime.utcnow)
    avatar_hash = db.Column(db.String(32))
    videos = db.relationship('Video', backref='author', lazy='dynamic')
    followed = db.relationship('Follow', foreign_keys=[Follow.follower_id], backref=db.backref('follower', lazy='joined'), lazy='dynamic', cascade='all, delete-orphan')
    followers = db.relationship('Follow', foreign_keys=[Follow.followed_id], backref=db.backref('followed', lazy='joined'), lazy='dynamic', cascade='all, delete-orphan')
    comments = db.relationship('Comment', backref='author', lazy='dynamic')
    notes = db.relationship('Note', backref='author', lazy='dynamic')
    transactions = db.relationship('Transaction', backref='author', lazy='dynamic')
    playlists = db.relationship('Playlist', backref='author', lazy='dynamic')

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({ 'confirm': self.id })

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id :
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)

        if self.email is not None and self.avatar_hash is None:
            self.avatar_hash = hashlib.md5(self.email.encode('utf-8')).hexdigest()

        self.followed.append(Follow(followed=self))

    @staticmethod
    def add_self_follows():
        for user in User.query.all():
            if not user.is_following(user):
                user.follow(user)
                db.session.add(user)
                db.session.commit()


    def ping(self):
        self.last_seen = datetime.utcnow()
        db.session.add(self)

    def gravatar(self, size=100, default='identicon', rating='g'):
        if request.is_secure:
            url = 'https://secure.gravatar.com/avatar'
        else:
            url = 'http://www.gravatar.com/avatar'
        hash = hashlib.md5(self.email.encode('utf-8')).hexdigest()
        return '{url}/{hash}?s={size}&d={default}&r={rating}'.format(url=url, hash=hash, size=size, default=default, rating=rating)


    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})



    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True


    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'])
        return s.dumps({'change_email': self.id, 'new_email': new_email})



    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id :
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        self.avatar_hash = hashlib.md5(self.email.encode('utf-8')).hexdigest()
        db.session.add(self)
        return True


    def follow(self, user):
        if not self.is_following(user):
            f = Follow(follower=self, followed=user)
            db.session.add(f)

    def unfollow(self, user):
        f = self.followed.filter_by(followed_id=user.id).first()
        if f:
            db.session.delete(f)

    def is_following(self, user):
        return self.followed.filter_by(followed_id=user.id).first() is not None

    def is_followed_by(self, user):
        return self.followers.filter_by(followed_id=user.id).first() is not None

    @property
    def followed_videos(self):
        return Video.query.join(Follow, Follow.followed_id == Video.author_id).filter(Follow.follower_id == self.id)


    def generate_auth_token(self, expiration):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id}).decode('ascii')

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])


    def to_json(self):
        json_user = {
            'url': url_for('api.get_video', id=self.id, _external=True),
            'username': self.username,
            'member_since': self.member_since,
            'last_seen': self.last_seen,
            'videos': url_for('api.get_user_videos', id=self.id, _external=True),
            'followed_videos': url_for('api.get_user_followed_videos', id=self.id, _external=True),
            'video_count': self.videos.count()
        }
        return json_user


    def __repr__(self):
        return '<User %r>' % self.username

class Video(db.Model):
    __tablename__ = 'videos'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    description = db.Column(db.Text)
    duration = db.Column(db.Integer)
    url = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    description_html = db.Column(db.Text)
    comments = db.relationship('Comment', backref='video', lazy='dynamic')
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
    notes = db.relationship('Note', backref='video', lazy='dynamic')
    transactions = db.relationship('Transaction', backref='video', lazy='dynamic')
    playlist_id = db.Column(db.Integer, db.ForeignKey('playlists.id'))

    def to_json(self):
        json_video = {
            'url': url_for('api.get_video', id=self.id, _external=True),
            'description': self.description,
            'description_html': self.description_html,
            'timestamp': self.timestamp,
            'author': url_for('api.get_user', id=self.author_id, _external=True),
            'comments': url_for('api.get_video_comments', id=self.id, _external=True),
            'comment_count': self.comments.count()
        }
        return json_video

    @staticmethod
    def from_json(json_video):
        description = json_video.get('description')
        if description is None or description == '':
            raise ValidationError('video does not have a description')
        return Video(description=description)

    @staticmethod
    def on_changed_description(target, value, oldvalue, initiator):
        allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'iframe', 'img', 'code', 'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul', 'h1', 'h2', 'h3', 'p']
        allowed_attrs = {'*': ['class'],
                        'a': ['href', 'rel'],
                        'img': ['src', 'alt']}
        target.description_html = bleach.linkify(bleach.clean(markdown(value, output_format='html'), tags=allowed_tags, strip=True, attributes=allowed_attrs))




class AnonymousUser(AnonymousUserMixin):
    pass


login_manager.anonymous_user = AnonymousUser
db.event.listen(Video.description, 'set', Video.on_changed_description)
db.event.listen(Comment.body, 'set', Comment.on_changed_body)
