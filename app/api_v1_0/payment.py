from flask_restful import Resource
from flask import g, request, current_app, url_for, jsonify
from flask_apispec import marshal_with, doc, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import Schema, fields
from ..models import Payment
from .. import db
from .errors import forbidden


class PaymentSchema(Schema):
    id = fields.Int(readable=True, dump_only=True)
    session_id = fields.Float(required=True, dump_only=True)
    timestamp = fields.DateTime(dump_only=True)
    mode = fields.Str(required=True, dump_only=True)
    payment_method_types = fields.List(fields.Str(), required=True, dump_only=True)

class PaymentApi(MethodResource, Resource):
    @marshal_with(PaymentSchema)
    @doc(tags=['payments'], summary='Get payments from an user', description='Get payments from an user')
    def get(self):
        try:
            page = request.args.get('page', 1, type=int)
            pagination = Payment.query.filter(author_id=g.current_user.id).paginate(page=page, per_page=current_app.config['VIDEOS_PER_PAGE'], error_out=False)
            payments = pagination.items
            prev = None
            if pagination.has_prev:
                prev = url_for('api.get_payments', page=page - 1, _external=True)
            next = None
            if pagination.has_next:
                next = url_for('api.get_payments', page=page + 1, _external=True)
            return jsonify({
                'payments': [payment.to_json() for payment in payments],
                'prev': prev,
                'next': next,
                'count': pagination.total
            })
        except Exception as e:
            return forbidden('Insufficient permissions')