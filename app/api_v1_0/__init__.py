from flask import Blueprint

api = Blueprint('api', __name__)

from app.api_v1_0 import authentication, video, comment, user, errors
