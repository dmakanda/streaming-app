from flask_restful import Resource
from flask import g, request, current_app, url_for, jsonify
from flask_apispec import marshal_with, doc, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import Schema, fields
from ..models import Transaction
from .. import db
from .errors import forbidden


class TransactionSchema(Schema):
    id = fields.Int(readable=True, dump_only=True)
    amount = fields.Float(required=True, dump_only=True)
    timestamp = fields.DateTime(dump_only=True)
    author_id = fields.Int(required=True, dump_only=True)
    video_id = fields.Int(required=True, dump_only=True)


class TransactionApi(MethodResource, Resource):
    @marshal_with(TransactionSchema)
    @doc(tags=['transactions'], summary='Get transactions from an user', description='Get transactions from an user')
    def get(self):
        try:
            page = request.args.get('page', 1, type=int)
            pagination = Transaction.query.filter(author_id=g.current_user.id).paginate(page=page, per_page=current_app.config['VIDEOS_PER_PAGE'], error_out=False)
            transactions = pagination.items
            prev = None
            if pagination.has_prev:
                prev = url_for('api.get_transations', page=page - 1, _external=True)
            next = None
            if pagination.has_next:
                next = url_for('api.get_transations', page=page + 1, _external=True)
            return jsonify({
                'transactions': [transaction.to_json() for transaction in transactions],
                'prev': prev,
                'next': next,
                'count': pagination.total
            })
        except Exception as e:
            return forbidden('Insufficient permissions')