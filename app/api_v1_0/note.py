from flask_restful import Resource
from flask_apispec import marshal_with, doc, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import Schema, fields
from ..models import Note
from .. import db


class NoteRequestSchema(Schema):
    rating = fields.Integer(required=True)
    author_id = fields.Integer(required=True)
    video_id = fields.Integer(required=True)

class NoteCreateApi(MethodResource, Resource):
    @doc(tags=['notes'], description="create a rating note for a video")
    @use_kwargs(NoteRequestSchema, location="json")
    def post(self, **kwargs):
        note = Note(
            rating=kwargs['rating'],
            author_id=kwargs['author_id'],
            video_id=kwargs['video_id']
        )
        db.session.add(note)
        db.session.commit()
        return note.to_json(), 201

class NoteApi(MethodResource, Resource):
    @doc(tags=['notes'], summary='Get notes for a video', description='Get note for a video')
    def get(self, video_id=None, author_id=None):
        note = Note.query.filter_by(video_id=video_id, author_id=author_id).first()
        if note is None:
            return { 'message': 'Note not found'}, 404
        return note.to_json(), 200

    @doc(tags=['notes'], summary='Create a note for a video', description='Create a note for a video')
    @use_kwargs(NoteRequestSchema, location="json")
    def put(self, video_id=None, author_id=None, **kwargs):
        note = Note.query.filter_by(video_id=video_id, author_id=author_id).first()
        if note is None:
             return {'message': 'Note not found'}, 404
        note.rating = kwargs['rating']
        db.session.commit()
        return note.to_json(), 200

    @doc(tags=['notes'], summary='Delete a note for a video', description='Delete a note for a video')
    def delete(self, video_id=None, author_id=None):
        note = Note.query.filter_by(video_id=video_id, author_id=author_id).first()
        if note is None:
            return {'message': 'Note not found'}, 404
        db.session.delete(note)
        db.session.commit()
        return {'message': 'Note deleted'}

