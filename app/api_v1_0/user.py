from flask import jsonify, request, current_app, url_for
from . import api
from ..models import User, Video

@api.route('/users/<int:id>')
def get_user(id):
    user = User.query.get_or_404(id)
    return jsonify(user.to_json())

@api.route('/users/<int:id>', methods=['PUT'])
def update_user(id):
    user = User.query.get_or_404(id)
    if 'username' in request.json:
        user.username = request.json['username']
    if 'email' in request.json:
        user.email = request.json['email']
    db.session.commit()
    return jsonify(user.to_json())

@api.route('/users/<int:id>/videos/')
def get_user_videos(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.videos.order_by(Video.timestamp.desc()).paginate(
        page, per_page=current_app.config['videos_PER_PAGE'],
        error_out=False)
    videos = pagination.items
    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_user_videos', page=page-1, _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_user_videos', page=page+1, _external=True)
    return jsonify({
        'videos': [video.to_json() for video in videos],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })


@api.route('/users/<int:id>/timeline/')
def get_user_followed_videos(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.followed_videos.order_by(Video.timestamp.desc()).paginate(
        page, per_page=current_app.config['videos_PER_PAGE'],
        error_out=False)
    videos = pagination.items
    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_user_followed_videos', page=page-1,
                       _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_user_followed_videos', page=page+1,
                       _external=True)
    return jsonify({
        'videos': [video.to_json() for video in videos],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })