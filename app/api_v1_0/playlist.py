from flask_restful import Resource
from flask_apispec import marshal_with, doc, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import Schema, fields
from ..models import Playlist
from .. import db



class PlaylistResponseSchema(Schema):
    message = fields.Str(default='Ok')

class PlaylistRequestSchema(Schema):
    user_id = fields.Int(required=True)
    name = fields.Str(required=True)
    is_private = fields.Boolean(required=False)

class PlaylistApi(MethodResource, Resource):
    @doc(tags=['playlists'], description="get playlist by id")
    def get(self, playlist_id):
        playlist = Playlist.query.filter_by(id=playlist_id).first()
        if playlist is None:
            return {'message': 'Playlist not found'}, 404
        return playlist.to_json(), 200

    @doc(tags=['playlists'], description="delete playlist by id")
    @marshal_with(PlaylistResponseSchema)
    def delete(self, playlist_id):
        query = db.session.query(Playlist).filter_by(id=playlist_id)
        playlist = query.first()
        if playlist is None:
            return {'message': 'Playlist not found'}, 404
        db.session.delete(playlist)
        return {'message': 'Playlist deleted'}, 200

    @doc(tags=['playlists'], description="update playlist by id")
    @use_kwargs(PlaylistRequestSchema, location="json")
    def put(self, playlist_id, **kwargs):
        playlist = Playlist.query.filter_by(id=playlist_id).first()
        if playlist is None:
            return {'message': 'Playlist not found'}, 404
        playlist.name = kwargs.get('name', playlist.name)
        playlist.is_private = kwargs.get('is_private', playlist.is_private)
        db.session.commit()
        return playlist.to_json(), 200

class PlaylistListApi(MethodResource, Resource):
    @doc(tags=['playlists'], description="get all playlists")
    def get(self):
        query = db.session.query(Playlist).all()
        return [ playlist.to_json() for playlist in query ], 200

    @doc(tags=['playlists'], description="create playlist")
    @use_kwargs(PlaylistRequestSchema, location="json")
    def post(self, **kwargs):
        playlist = Playlist(**kwargs)
        db.session.add(playlist)
        db.session.commit()
        return playlist.to_json(), 201
