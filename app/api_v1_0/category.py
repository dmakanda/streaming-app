from flask_restful import Resource
from flask_apispec import marshal_with, doc, use_kwargs
from flask_apispec.views import MethodResource
from marshmallow import Schema, fields
from ..models import Category
from .. import db


class CategorySchema(Schema):
    id = fields.Int(readable=True, dump_only=True)
    name = fields.Str(required=True)


class CategoryApi(MethodResource, Resource):
    @doc(tags=['categories'], description='Get all categories')
    def get(self):
        return [category.to_json() for category in Category.query.all()]

    @use_kwargs(CategorySchema, location="json")
    @doc(tags=['categories'], description='Create a new category')
    def post(self, name):
        category = Category(name=name)
        if not db.session.query(Category).filter_by(name=name).first():
            db.session.add(category)
            db.session.commit()
            return category.to_json(), 201
        return {'message': f'{category.name} already exists'}, 400
