import os
from flask import Flask, render_template, jsonify
from flask_assets import Bundle, Environment
from flask_mail import Mail
from flask_login import LoginManager
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from flask_pagedown import PageDown
from flask_flatpages import FlatPages
from flask_restful import Api
from flask_sitemap import Sitemap
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec
from config import config, Config

mail = Mail()
moment = Moment()
pagedown = PageDown()
pages = FlatPages()
db = SQLAlchemy()
ext = Sitemap()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'


def create_app(config_name=Config):
    app = Flask(__name__)
    app.config['CORS_HEADERS'] = 'Access-Control-Allow-Origin'
    assets = Environment(app)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        from flask_sslify import SSLify
        sslify = SSLify(app)

    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    pagedown.init_app(app)
    pages.init_app(app)
    ext.init_app(app)
    api = Api(app)
    
    app.config.update({
        'APISPEC_SPEC': APISpec(
            title='Streaming API',
            version='v1',
            plugins=[MarshmallowPlugin()],
            openapi_version='2.0.0'
        ),
        'APISPEC_SWAGGER_URL': '/swagger/',  # URI to access API Doc JSON 
        'APISPEC_SWAGGER_UI_URL': '/swagger-ui/'  # URI to access UI of API Doc
    })
    docs = FlaskApiSpec(app)
    
    from app.hello import bp as hello_blueprint
    from .index import index as index_blueprint
    from .api_v1_0 import api as api_1_0_blueprint
    app.register_blueprint(index_blueprint)
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1.0')
    app.register_blueprint(hello_blueprint, url_prefix='/hello')

    from .api_v1_0.playlist import PlaylistApi, PlaylistListApi
    from .api_v1_0.note import NoteApi, NoteCreateApi
    from .api_v1_0.category import CategoryApi
    from .api_v1_0.transaction import TransactionApi
    from .api_v1_0.payment import PaymentApi
    api.add_resource(PlaylistApi, '/api/v1.0/playlists/<playlist_id>', methods=['GET', 'PUT', 'DELETE'])
    api.add_resource(PlaylistListApi, '/api/v1.0/playlists', methods=['GET', 'POST'])
    api.add_resource(NoteCreateApi, '/api/v1.0/notes', methods=['POST'])
    api.add_resource(CategoryApi, '/api/v1.0/categories', methods=['GET', 'POST'])
    api.add_resource(TransactionApi, '/api/v1.0/transactions', methods=['GET'])
    api.add_resource(PaymentApi, '/api/v1.0/payments', methods=['GET'])
    api.add_resource(NoteApi, '/api/v1.0/notes/<video_id>/<author_id>', methods=['GET', 'PUT', 'DELETE'])

    docs.register(PlaylistApi)
    docs.register(PlaylistListApi)
    docs.register(NoteApi)
    docs.register(CategoryApi)
    docs.register(TransactionApi)
    docs.register(NoteCreateApi)
    docs.register(PaymentApi)


    root_js = Bundle()
    root_css = Bundle()

    assets.register('root_js', root_js)
    assets.register('root_css', root_css)

    return app

